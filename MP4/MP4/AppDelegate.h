//
//  AppDelegate.h
//  MP4
//
//  Created by Sathish Kumar Aruchamy on 08/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
