//
//  GamesTableViewController.m
//  MP4
//
//  Created by Sathish Kumar Aruchamy on 08/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "GamesTableViewController.h"
#import "GameController.h"

@interface GamesTableViewController ()

@end

@implementation GamesTableViewController
{
    NSMutableArray *yourTurn;
    NSMutableArray *theirTurn;
    NSMutableArray *availableGames;
    NSMutableArray *finishedGames;
    
    
}

- (void) viewDidLoad{
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor grayColor];
    
    [refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventAllEvents];
    self.refreshControl = refreshControl;
    
    //[self performSegueWithIdentifier:@"showGame" sender:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([PFUser currentUser]) {
        self.title = [NSString stringWithFormat:NSLocalizedString(@"Connect4 - %@!", nil), [[PFUser currentUser] username]];
    } else {
        self.title = NSLocalizedString(@"Not logged in", nil);
    }
}

-(void) refreshView{
    [self getQuery];
    [self stopRefresh];
}

- (void)stopRefresh
{
    [self.refreshControl endRefreshing];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (![PFUser currentUser]) { // No user logged in
        // Create the log in view controller
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Create the sign up view controller
        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        [signUpViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Assign our sign up controller to be displayed from the login controller
        [logInViewController setSignUpController:signUpViewController];
        
        // Present the log in view controller
        [self presentViewController:logInViewController animated:YES completion:NULL];
    }
    else{
        [self getQuery];
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section ==0)
        return  yourTurn.count;
    else if(section ==1)
        return  theirTurn.count;
    else if(section ==2)
        return  availableGames.count;
    else
        return finishedGames.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    //UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"Cell%d%d", indexPath.row, indexPath.section]];
    }
    PFObject *player1Object;
    PFObject *player2Object;
    
    if(indexPath.section !=3) {
        UIView *player1Color= [[UIView alloc] initWithFrame:CGRectMake(40,0, 40,40)];
        player1Color.alpha = 0.5;
        player1Color.layer.cornerRadius = 20;
        [player1Color setBackgroundColor:[UIColor yellowColor]];
        
        UILabel *player1= [[UILabel alloc] initWithFrame:CGRectMake(100, 8.0, 40, 20)];
        [player1 setFont:[UIFont systemFontOfSize:14.0]];
        [player1 setTextColor:[UIColor blackColor]];
        [player1 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        UILabel *vs = [[UILabel alloc] initWithFrame:CGRectMake(150, 8.0, 20, 20)];
        [vs setFont:[UIFont boldSystemFontOfSize:17.0]];
        [vs setTextColor:[UIColor blackColor]];
        [vs setTextAlignment:NSTextAlignmentCenter];
        [vs setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        vs.text = @"vs";
        
        
        UIView *player2Color = [[UIView alloc] initWithFrame:CGRectMake(330,0, 40,40)];
        player2Color.alpha = 0.5;
        player2Color.layer.cornerRadius = 20;
        [player2Color setBackgroundColor:[UIColor redColor]];
        
        UILabel *player2 = [[UILabel alloc] initWithFrame:CGRectMake(380, 8.0, 40, 20)];
        [player2 setFont:[UIFont systemFontOfSize:14.0]];
        [player2 setTextColor:[UIColor blackColor]];
        [player2 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        if(indexPath.section ==0){
            player1Object = yourTurn[indexPath.row][@"player1"];
            player1.text = player1Object[@"username"];
            player2Object = yourTurn[indexPath.row][@"player2"];
            if(player2Object != nil)
                player2.text = player2Object[@"username"];
            else
                player2.text = @"(No opponent)";
        }
        else if(indexPath.section == 1)
        {
            player1Object = theirTurn[indexPath.row][@"player1"];
            player1.text = player1Object[@"username"];
            player2Object = theirTurn[indexPath.row][@"player2"];
            if(player2Object != nil)
                player2.text = player2Object[@"username"];
            else
                player2.text = @"(No opponent)";
        }
        else{
            
            player1Object = availableGames[indexPath.row][@"player1"];
            player1.text = player1Object[@"username"];
            player2Object = availableGames[indexPath.row][@"player2"];
            if(player2Object != nil)
                player2.text = player2Object[@"username"];
            else
                player2.text = @"(No opponent)";
            
        }
        
        
        
        [cell addSubview:player1Color];
        [cell addSubview:player2Color];
        [cell addSubview:player2];
        [cell addSubview:player1];
        [cell addSubview:vs];
    }
    else{
        
        UILabel *label= [[UILabel alloc] initWithFrame:CGRectMake(20, 8.0, 300, 20)];
        [label setFont:[UIFont systemFontOfSize:17.0]];
        [label setTextColor:[UIColor darkGrayColor]];
        [label setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        player1Object = finishedGames[indexPath.row][@"player1"];
        player2Object = finishedGames[indexPath.row][@"player2"];
        
        if([player1Object.objectId isEqualToString:[PFUser currentUser].objectId])
            label.text = [NSString stringWithFormat:@"Game with %@", player2Object[@"username"]];
        else
            label.text = [NSString stringWithFormat:@"Game with %@", player1Object[@"username"]];
        
        
        UILabel *label1= [[UILabel alloc] initWithFrame:CGRectMake(340, 8.0, 100, 20)];
        [label1 setFont:[UIFont systemFontOfSize:17.0]];
        [label1 setTextColor:[UIColor darkGrayColor]];
        [label1 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        player1Object = finishedGames[indexPath.row][@"winner"];
        
        
        if([player1Object.objectId isEqualToString:[PFUser currentUser].objectId])
            label1.text = @"You Won";
        else
            label1.text = [NSString stringWithFormat:@"%@ Won", player1Object[@"username"]];
        
        
        [cell addSubview:label];
        [cell addSubview:label1];
        
        
        
    }

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   // NSLog(@"%d - %d", indexPath.section, indexPath.row);
        [self performSegueWithIdentifier:@"showGame" sender:self];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section ==0)
        return  @"YOUR TURN";
    else if(section ==1)
        return  @"THEIR TURN";
    else if(section ==2)
        return  @"AVAILABLE GAMES";
    else
        return @"FINISHED GAMES";
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue.identifier description] isEqualToString:@"showGame"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        int section = indexPath.section;
        PFObject *boardObject;
        PFObject *gameObject;
        NSString *gameState;
        if(section ==0){
            gameObject = yourTurn[indexPath.row];
            boardObject = yourTurn[indexPath.row][@"board"];
            gameState = @"ONGOING_1";
        }
        else if(section ==1){
            gameObject = theirTurn[indexPath.row];
            boardObject = theirTurn[indexPath.row][@"board"];
            gameState = @"ONGOING_2";
        }
        else if(section ==2){
            gameObject = availableGames[indexPath.row];
            boardObject = availableGames[indexPath.row][@"board"];
            gameState = @"NEW_PLAYER";
        }
        else if(section ==3){
            gameObject = finishedGames[indexPath.row];
            boardObject = finishedGames[indexPath.row][@"board"];
            gameState = @"COMPLETED";
        }
        
        GameController *controller = segue.destinationViewController;
        controller.boardObject = boardObject;
        controller.gameObject = gameObject;
        controller.gameState = gameState;
        
    }
}



#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    NSLog(@"User dismissed the logInViewController");
}


#pragma mark - PFSignUpViewControllerDelegate

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || !field.length) { // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}


- (IBAction)logOut:(id)sender {
    [PFUser logOut];
    PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate

    [self presentViewController:logInViewController animated:YES completion:NULL];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)newGame:(id)sender {
    
    if(![self checkForAvailableGames]){
        
        PFUser *user = [PFUser currentUser];
        PFObject *gameObject = [PFObject objectWithClassName:@"Game"];
        gameObject[@"player1"] = user;
        gameObject[@"completed"] = @0;
        gameObject[@"turn"] = user;
        gameObject[@"turnNumber"] = @1;
        [gameObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(succeeded && !error){
                [yourTurn addObject:gameObject];
                [self.tableView reloadData];
            }
        }];
    }
    
    
}

-(BOOL) checkForAvailableGames{
    if(availableGames.count > 0)
    {
        [yourTurn addObject:[availableGames objectAtIndex:0]];
        [availableGames removeObjectAtIndex:0];
        
        PFObject *availableObject  =[yourTurn objectAtIndex:0];
        availableObject[@"player2"] = [PFUser currentUser];
        if(availableObject[@"turn"] == nil)
            availableObject[@"turn"] = [PFUser currentUser];
        [availableObject saveInBackground];
        
        [self.tableView reloadData];
        return  YES;
    }
    else
        return  NO;
}

-(void) getQuery{
    
    PFUser *user = [PFUser currentUser];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat: @"%@ = player1 OR %@ = player2 OR completed = 1",user,user];
    PFQuery *gameQuery = [PFQuery queryWithClassName:@"Game"];
    [gameQuery includeKey:@"board"];
    [gameQuery includeKey:@"player1"];
    [gameQuery includeKey:@"player2"];
    [gameQuery includeKey:@"turn"];
    [gameQuery includeKey:@"winner"];
    
    [gameQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            yourTurn = [[NSMutableArray alloc] init];
            theirTurn = [[NSMutableArray alloc] init];
            availableGames = [[NSMutableArray alloc] init];
            finishedGames = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                PFObject *player1 = object[@"player1"];
                PFObject *player2 = object[@"player2"];
                PFObject *turn = object[@"turn"];
                
                
                if([user.objectId isEqualToString:turn.objectId] && [object[@"completed"] intValue] != 1){
                    //Your Turn
                    [yourTurn addObject:object];
                    
                }
                else if (![user.objectId isEqualToString:turn.objectId]
                         && ([user.objectId isEqualToString:player1.objectId]
                             || [user.objectId isEqualToString:player2.objectId])
                         && [object[@"completed"] intValue] != 1)
                {
                    //their turn
                    [theirTurn addObject:object];
                    
                }
                else if (![user.objectId isEqualToString:player1.objectId] && player2 == nil)
                {
                    //available game
                    [availableGames addObject:object];
                    
                }
                else if ([object[@"completed"] intValue] == 1)
                {
                    //Completed game
                    [finishedGames addObject:object];
                    
                }
                
            }
            [self.tableView reloadData];
        }
        else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    
}

-(IBAction) gameOver:(UIStoryboardSegue *) segue{
    [self.tableView reloadData];
}

@end
