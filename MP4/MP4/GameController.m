//
//  ViewController.m
//  MP3
//
//  Created by Karthik Balasubramanian on 07/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "GameController.h"

@interface GameController ()

@end

@implementation GameController {
    NSMutableArray *pieces;
    BOOL animating;
    BOOL FREEZE_MODE;
    NSTimer *timer;
    BOOL flag;
   
}

- (void) awakeFromNib{
    
    self.gameModel = [[GameModel alloc] init];
    
    pieces = [[NSMutableArray alloc] init];
}


- (void) viewDidLoad {
    
    [super viewDidLoad];
    FREEZE_MODE = NO;
    
    if([self.gameState isEqualToString:@"COMPLETED"]){
        FREEZE_MODE =YES;
        self.gameModel.boardObject = self.boardObject;
        self.gameModel.gameObject = self.gameObject;
        
    }
    else if([self.gameState isEqualToString:@"NEW_PLAYER"])
    {
        self.gameObject[@"player2"] = [PFUser currentUser];
        self.gameObject[@"turn"] = [PFUser currentUser];
        
        [self.gameObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            self.gameModel.boardObject = self.boardObject;
            self.gameModel.gameObject = self.gameObject;
        }];
        
    }
    
    else{
        self.gameModel.boardObject = self.boardObject;
        self.gameModel.gameObject = self.gameObject;
    }
    
     timer = [NSTimer timerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(refreshBoard)
                                           userInfo:nil
                                            repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}




- (void) resetGame{
    [self.gameModel resetGame];
    
    for(UIView * piece in pieces){
        [piece removeFromSuperview];
    }
    [pieces removeAllObjects];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    flag = YES;
    
    self.boardView = [[BoardView alloc] initWithFrame:
                      CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
                      slotDiameter:30];
    //self.boardView = [self.boardView initWithFrame:self.view.bounds slotDiameter:30];
    self.boardView.delegate =self;
    [self.view addSubview:self.boardView];
    [self setTitle];
    if(self.boardObject!=nil)
    {
        [self addPieces];
    }
    
}

- (void) setTitle{
    
    UIView *player1Color= [[UIView alloc] initWithFrame:CGRectMake(0,0, 30,30)];
    player1Color.alpha = 0.5;
    player1Color.layer.cornerRadius = 15;
    [player1Color setBackgroundColor:[UIColor yellowColor]];
    
    
    
    UILabel *player1= [[UILabel alloc] initWithFrame:CGRectMake(40, 8.0, 80, 20)];
    [player1 setFont:[UIFont systemFontOfSize:12.0]];
    [player1 setTextColor:[UIColor blackColor]];
    [player1 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    player1.text = self.gameObject[@"player1"][@"username"];
    
    UILabel *vs = [[UILabel alloc] initWithFrame:CGRectMake(110, 8.0, 20, 20)];
    [vs setFont:[UIFont boldSystemFontOfSize:12.0]];
    [vs setTextColor:[UIColor blackColor]];
    [vs setTextAlignment:NSTextAlignmentCenter];
    [vs setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    vs.text = @"vs";
    
    
    UIView *player2Color = [[UIView alloc] initWithFrame:CGRectMake(160,0, 30,30)];
    player2Color.alpha = 0.5;
    player2Color.layer.cornerRadius = 15;
    [player2Color setBackgroundColor:[UIColor redColor]];
    
    UILabel *player2 = [[UILabel alloc] initWithFrame:CGRectMake(200, 8.0, 80, 20)];
    [player2 setFont:[UIFont systemFontOfSize:12.0]];
    [player2 setTextColor:[UIColor blackColor]];
    [player2 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
    PFObject *player2Object = self.gameObject[@"player2"];
    if(player2Object != nil)
        player2.text = player2Object[@"username"];
    else
        player2.text = @"(No opponent)";
    
    

    
    [self.titleView addSubview:player1Color];
    [self.titleView addSubview:player2Color];
    [self.titleView addSubview:player2];
    [self.titleView addSubview:player1];
    [self.titleView addSubview:vs];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *modeTitle =[alertView buttonTitleAtIndex:buttonIndex];
    
    if([modeTitle isEqualToString:@"Ok"]){
        [self performSegueWithIdentifier:@"gameOver" sender:nil];
    }

}


- (void)boardView:(BoardView *)boardView columnSelected:(int)column{
    if(!FREEZE_MODE && [[self.gameObject[@"turn"] objectId] isEqualToString:[PFUser currentUser].objectId])
        [self processTurn:column];
    
}

-(void) processTurn:(int) column{
    
    if(animating)
        return;
    
    if([self.gameModel processTurnAtColumn:column]){
        
        UIView *piece = [[UIView alloc] initWithFrame:CGRectMake((column + 1) * self.boardView.gridWidth - self.boardView.slotDiameter / 2.0, - self.boardView.slotDiameter, self.boardView.slotDiameter,self.boardView.slotDiameter)];
        
        [pieces addObject:piece];
        
        piece.backgroundColor = [self.gameModel currentColor]; // Ask for peice and get color
        
        [self.view insertSubview:piece belowSubview:self.boardView];
        
        animating = YES ;
        [UIView animateWithDuration:0.5 animations:^{
            // 5 is the position to get the circle . Need to be dynamic
            piece.center = CGPointMake((column + 1) * self.boardView.gridWidth, (6 - ([self.gameModel topEmptyRowInCol:column] -1 )) * self.boardView.gridHeight);
            
        }completion:^(BOOL finished) {
            animating = NO;
            if(self.gameModel.gameOver){
                
                
                NSString *winnerString ;
                
                PFUser *winPlayer = (PFUser *)self.gameObject[@"winner"];
                
                if([winPlayer.objectId isEqualToString:[PFUser currentUser].objectId])
                    winnerString = [NSString stringWithFormat:@"You Won"];
                else
                    winnerString = [NSString stringWithFormat:@"%@ Won",self.gameObject[@"winner"][@"username"]];
                flag = NO;
                [[[UIAlertView alloc] initWithTitle:@"Game Over"
                                            message:winnerString
                                           delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil] show];
                [timer invalidate];
                [self resetGame];
                
                //[[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    
                //}];
                
                //return;
            }
         
            
        }];
        
    }
}


- (void) addPieces{
    NSMutableArray *board = self.gameModel.restoreGame;
    if(board != nil){
        for(int i =0; i< board.count ; i ++)
        {
            NSMutableArray *colarray = [board objectAtIndex:i];
            for(int j =0; j< colarray.count ; j++)
            {
                int value = [[colarray objectAtIndex:j] intValue];
                if(value > 0 )
                {
                    UIView *piece = [[UIView alloc] initWithFrame:CGRectMake((j + 1) * self.boardView.gridWidth - self.boardView.slotDiameter / 2.0, - self.boardView.slotDiameter, self.boardView.slotDiameter,self.boardView.slotDiameter)];
                    [pieces addObject:piece];
                    
                    if(value == 1)
                        piece.backgroundColor = [UIColor yellowColor];
                    else
                        piece.backgroundColor = [UIColor redColor];
                    
                    piece.center = CGPointMake((j + 1) * self.boardView.gridWidth, (6 - (i)) * self.boardView.gridHeight);
                    [self.view insertSubview:piece belowSubview:self.boardView];

                    
                    
                }
                

            }
        }
        
        
    }
}

- (void)refreshBoard {
    
    if(flag)
    {
        
        PFQuery *gameQuery = [PFQuery queryWithClassName:@"Game"];
        [gameQuery includeKey:@"board"];
        [gameQuery includeKey:@"player1"];
        [gameQuery includeKey:@"player2"];
        [gameQuery includeKey:@"turn"];
        [gameQuery includeKey:@"winner"];
        [gameQuery getObjectInBackgroundWithId:self.gameObject.objectId block:^(PFObject *object, NSError *error) {
            if(!error){
                self.gameObject = object;
                self.gameModel.gameObject = object;
                self.boardObject = object[@"board"];
                self.gameModel.boardObject = object[@"board"];
                
                
                [self addPieces];
                
                
                if([self.gameObject[@"completed"] intValue] == 1)
                {
                    NSString *winnerString ;
                    PFObject *winPlayer = self.gameObject[@"winner"];
                    
                    if([winPlayer.objectId isEqualToString:[PFUser currentUser].objectId])
                        winnerString = [NSString stringWithFormat:@"You Won"];
                    else
                        winnerString = [NSString stringWithFormat:@"%@ Won",self.gameObject[@"winner"][@"username"]];
                    
                    [[[UIAlertView alloc] initWithTitle:@"Game Over"
                                                message:winnerString
                                               delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil] show];
                    [timer invalidate];
                    
                }

            }
        }];
    
        
    }
}

@end
