//
//  GameModel.m
//  Connect4
//
//  Created by Karthik Balasubramanian on 5/2/14.
//  Copyright (c) 2014 Karthik Balasubramanian. All rights reserved.
//


///Things to do
// Turn logic , how to identify a turn
//Query for current turn and board


#import "GameModel.h"

@implementation GameModel{

    int _turn;
    int _winner;
    int _pieces[6][7];
    int _numPiecesInCol[7];

}

- (id) init {
    if(self = [super init]){
        _turn = 1;
    }
    return self;
}

- (int) turn{
    
    return _turn;
        
}

- (UIColor *) currentColor{
    
    if(_turn == 1)
        return [UIColor redColor];
    else
        return [UIColor yellowColor];
    
}


- (BOOL) gameOver{
    
    return _winner !=0;
}


- (BOOL) checkWinAtColumn:(int) col WithTurn:(int) turn {
    
    int row = [self topEmptyRowInCol:col];
    if ([self checkWin:row-3 Rowend:row+4 ColStart:col-3 Colend:col+4])
        return YES;
    else
        
        if ([self checkWin:row+3 Rowend:row-4 ColStart:col-3 Colend:col+4])
            return YES;
        else
            
            if ([self checkWin:row Rowend:row ColStart:col-3 Colend:col+4])
                return YES;
            else
                
                if ([self checkWin:row-3 Rowend:row+4 ColStart:col Colend:col])
                    return YES;
    
    return NO;
    
}



- (BOOL) checkWin:(int) startRow Rowend:(int) endRow  ColStart:(int)startCol  Colend:(int)endCol  {
    
        int rowStep = (startRow > endRow ? -1 : (startRow < endRow ? 1 : 0));
        int colStep = (startCol > endCol ? -1 : (startCol < endCol ? 1 : 0));
        int count = 0;
        int row = startRow;
        int col = startCol;
        
        for (; row != endRow || col != endCol; row += rowStep, col += colStep) {
           
            if (row < 0 || col < 0 || row >= 6 || col >= 7)
                continue;
            if (_pieces[row][col] == _turn)
                count ++;
            else
                count = 0;
            
            if (count == 4) break;
        }
        
        return (count == 4);

}
- (int) winner{
    
    return _winner;
}

- (BOOL) processTurnAtColumn:(int)col   {
    
    
    if(_numPiecesInCol[col] == 6 )
        return NO;
    
    _pieces[_numPiecesInCol[col]][col]=_turn;
    

    if([self checkWinAtColumn:col WithTurn:_turn])
        _winner = _turn;
    _numPiecesInCol[col]++;
    _turn = (_turn == 1) ? 2 :1 ;

    [self saveGameModel];
    
    return YES;
}


- (int) topEmptyRowInCol:(int)col{
    return _numPiecesInCol[col];
}

- (int) pieceForRow:(int)row col:(int)col{
    return 0;
}

- (void) resetGame{
    _turn =1;
    _winner =0;
    for(int i =0; i< 6 ; i ++)
    {
        _numPiecesInCol[i] = 0;
    }
    
    for(int i =0; i< 6 ; i ++)
    {
        for(int j =0; j< 7 ; j++)
        {
            _pieces[i][j] = 0;
        }
    }
}

- (void) saveGameModel{
    
    if(self.boardObject == nil) {
        self.boardObject = [PFObject objectWithClassName:@"Board"];
        
    }
    
    self.boardObject[@"pieces"] =[self convertIntToMutableArray:_pieces];
    self.boardObject[@"piecesincol"] = [self convertIntToArray:_numPiecesInCol];
    [self.boardObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(!error){
            if(_turn == 1)
                self.gameObject[@"turn"] = self.gameObject[@"player1"];
            else{
                if(self.gameObject[@"player2"] ==nil)
                    [self.gameObject removeObjectForKey:@"turn"];
                else
                    self.gameObject[@"turn"] = self.gameObject[@"player2"];
            }
            self.gameObject[@"turnNumber"] = [NSNumber numberWithInt:_turn];
            
            if(_winner != 0)
            {
                if(_winner == 1)
                    self.gameObject[@"winner"] = self.gameObject[@"player1"];
                else
                    self.gameObject[@"winner"] = self.gameObject[@"player2"];
                
                self.gameObject[@"completed"] = @1;
            }
            self.gameObject[@"board"] = self.boardObject;
            [self.gameObject saveInBackground];
        }
    }];
    

}

- (NSMutableArray *) convertIntToArray:(NSInteger[7]) numpieces{
    NSMutableArray *rowArray = [[NSMutableArray alloc] initWithCapacity:6];
    
    for(int i=0; i < 7; i++) {
        [rowArray insertObject:[NSNumber numberWithInteger:numpieces[i]] atIndex:i];
    }
    
    return rowArray;
}


- (void) convertArrayToInt:(NSMutableArray *) pieceArray{
  
        for(int j =0; j< pieceArray.count ; j++)
        {
            _numPiecesInCol[j] = [[pieceArray objectAtIndex:j] intValue];
        }
    
}

- (NSMutableArray*) convertIntToMutableArray:(NSInteger[6][7]) pieces{
    NSMutableArray *rowArray = [[NSMutableArray alloc] initWithCapacity:6];
    
    for(int i=0; i < 6; i++) {
        [rowArray insertObject:[NSMutableArray arrayWithObjects:
                                [NSNumber numberWithInteger:pieces[i][0]],
                                [NSNumber numberWithInteger:pieces[i][1]],
                                [NSNumber numberWithInteger:pieces[i][2]],
                                [NSNumber numberWithInteger:pieces[i][3]],
                                [NSNumber numberWithInteger:pieces[i][4]],
                                [NSNumber numberWithInteger:pieces[i][5]],
                                [NSNumber numberWithInteger:pieces[i][6]],
                                nil] atIndex:i];
    }
    
    return rowArray;
}

- (void) convertMutableArrayToInt:(NSMutableArray *) pieceArray{
    for(int i =0; i< pieceArray.count ; i ++)
    {
        NSMutableArray *colarray = [pieceArray objectAtIndex:i];
        for(int j =0; j< colarray.count ; j++)
        {
            _pieces[i][j] = [[colarray objectAtIndex:j] intValue];
        }
    }
}

- (NSMutableArray *) restoreGame{
    if(self.boardObject != nil){
        NSMutableArray *board = self.boardObject[@"pieces"];
        [self convertMutableArrayToInt:board];
        NSMutableArray *numPieces = self.boardObject[@"piecesincol"];
        [self convertArrayToInt:numPieces];
        _turn = [self.gameObject[@"turnNumber"] intValue];
        return board;
    }
    return nil;
}


@end
