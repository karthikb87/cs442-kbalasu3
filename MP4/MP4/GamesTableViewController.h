//
//  GamesTableViewController.h
//  MP4
//
//  Created by Sathish Kumar Aruchamy on 08/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellView.h"
#import  <Parse/Parse.h>

@interface GamesTableViewController : UITableViewController<PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate>
- (IBAction)logOut:(id)sender;

- (IBAction)newGame:(id)sender;

@end
