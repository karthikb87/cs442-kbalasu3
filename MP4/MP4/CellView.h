//
//  CellView.h
//  MP4
//
//  Created by Sathish Kumar Aruchamy on 08/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellView : UITableViewCell
@property (strong, nonatomic) UILabel *player1;
@property (strong, nonatomic) UILabel *player2;
@property (strong, nonatomic) UIView *view1;
@property (strong, nonatomic) UIView *view2;
@property (strong, nonatomic) UILabel *vs;
@end
