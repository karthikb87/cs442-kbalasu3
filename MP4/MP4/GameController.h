//
//  ViewController.h
//  MP4
//
//  Created by Karthik Balasubramanian on 07/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
#import "GameModel.h"
#import <Parse/Parse.h>

@interface GameController : UIViewController <BoardViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet BoardView *boardView;
@property (strong, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) GameModel *gameModel;
@property (strong ,nonatomic) PFObject *boardObject;
@property (strong ,nonatomic) PFObject *gameObject;
@property (strong, nonatomic) NSString *gameState;
@end
