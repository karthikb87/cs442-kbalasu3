//
//  CellView.m
//  MP4
//
//  Created by Sathish Kumar Aruchamy on 08/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "CellView.h"

@implementation CellView

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGSize size = self.contentView.frame.size;
        
       /* // Initialize Main Label
        self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(8.0, 8.0, size.width - 16.0, size.height - 16.0)];
        
        // Configure Main Label
        [self.mainLabel setFont:[UIFont boldSystemFontOfSize:24.0]];
        [self.mainLabel setTextAlignment:NSTextAlignmentCenter];
        [self.mainLabel setTextColor:[UIColor orangeColor]];
        [self.mainLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        */
        
        self.view1 = [[UIView alloc] initWithFrame:CGRectMake(40,0, 40,40)];
        self.view1.alpha = 0.5;
        self.view1.layer.cornerRadius = 20;
        //self.view1.backgroundColor = [UIColor redColor];
        
        
        self.player1 = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 20, 20)];
        [self.player1 setFont:[UIFont boldSystemFontOfSize:12.0]];
        [self.player1 setTextColor:[UIColor blackColor]];
        [self.player1 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        self.vs = [[UILabel alloc] initWithFrame:CGRectMake(230, 0, 20, 20)];
        [self.vs setFont:[UIFont boldSystemFontOfSize:12.0]];
        [self.vs setTextColor:[UIColor blackColor]];
        [self.vs setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        self.player2 = [[UILabel alloc] initWithFrame:CGRectMake(size.width+20, 0, 20, 20)];
        [self.player2 setFont:[UIFont boldSystemFontOfSize:12.0]];
        [self.player2 setTextColor:[UIColor blackColor]];
        [self.player2 setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
        
        
        self.view2 = [[UIView alloc] initWithFrame:CGRectMake(size.width + 60,0, 40,40)];
        self.view2.alpha = 0.5;
        self.view2.layer.cornerRadius = 20;
        
        
        
        
        // Add Main Label to Content View
        [self.contentView addSubview:self.view1];
        [self.contentView addSubview:self.view2];
        [self.contentView addSubview:self.player1];
        [self.contentView addSubview:self.player2];
        [self.contentView addSubview:self.vs];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:NO];


    // Configure the view for the selected state
}

@end
