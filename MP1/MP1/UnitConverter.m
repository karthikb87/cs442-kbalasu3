//
//  UnitConverter.m
//  MP1
//
//  Created by Karthik Balasubramanian on 01/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "UnitConverter.h"

@implementation UnitConverter
- (double) degreeConvert:(double)valueToConvert Celcius:(bool)toCelcius
{
    double result = 0.0;
    if(toCelcius == YES){
        result = (valueToConvert -32) * 5/9;
    }
    else{
        result = (valueToConvert * 9/5) + 32;
    }
    return result;
}

- (double) distanceConvert:(double)valueToConvert km:(bool)tokm
{
    double result = 0.0;
    double convert = 0.62137;
    if(tokm == YES){
        result = (valueToConvert * 1/convert);
    }
    else{
        result = (valueToConvert * convert);
    }
    return result;
}

- (double) volumeConvert:(double)valueToConvert litres:(bool)tolitres
{
    double result = 0.0;
    double convert = 3.78541;
    if(tolitres == YES){
        result = (valueToConvert * convert);
    }
    else{
        result = (valueToConvert *  1/convert);
    }
    return result;
}

@end
