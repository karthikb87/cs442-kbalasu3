//
//  UnitConverter.h
//  MP1
//
//  Created by Karthik Balasubramanian on 01/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnitConverter : NSObject
- (double) degreeConvert:(double)valueToConvert Celcius:(bool)toCelcius;
- (double) distanceConvert:(double)valueToConvert km:(bool)tokm;
- (double) volumeConvert:(double)valueToConvert litres:(bool)tolitres;
@end
