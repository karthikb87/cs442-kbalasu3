//
//  ViewController.m
//  MP1
//
//  Created by Karthik Balasubramanian on 01/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "UnitConverterController.h"
#import "UnitConverter.h"
@interface UnitConverterController ()

@end

@implementation UnitConverterController
UnitConverter *unitConv;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.topToBottom = YES;
    unitConv = [[UnitConverter alloc] init];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (IBAction)buttonInside:(id)sender {
    [self convertToUnits];
}

//Touches began event to hide keyboard when user touches background
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event  allTouches] anyObject];
    if([_topFieldText isFirstResponder] && [touch view] != _topFieldText){
        [_topFieldText resignFirstResponder];
    }
    else if([_bottomFieldText isFirstResponder] && [touch view] != _bottomFieldText){
        [_bottomFieldText resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

//Method to hide keyboard when user hits enter in textField
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//Automatically changing the text of the button on selection of textfield
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSString *buttonText;
    NSInteger textIndex = [textField tag];
    if(textIndex == 1){
        self.topToBottom = YES;
         buttonText =@"▼";
    }
    else{
        self.topToBottom = NO;
        buttonText =@"▲";
    }
    [self.convertTo setTitle:buttonText forState:UIControlStateNormal];
    //NSLog(@"%d",[textField tag]);
}


//Segmented tab changed event fires when the user changes the tab. Here when the
// user changes the tab the label text is modified
- (IBAction)tabChanged:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl * ) sender;
    NSInteger selectedIndex =segmentedControl.selectedSegmentIndex;
    if(selectedIndex ==0){
        self.topLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ºF", nil)];
        self.bottomLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ºC", nil)];
    }
    else if (selectedIndex ==1){
        self.topLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Miles", nil)];
        self.bottomLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Kilometres", nil)];
    }
    else
    {
        self.topLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Gallons", nil)];
        self.bottomLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Litres", nil)];
    }
    [self clearFields];
    //NSLog(@"%d",segmentedControl.selectedSegmentIndex);
}

//unit conversion happens here
- (IBAction)convertTo:(id)sender {
    NSString *buttonText = [self returnButtonText];
    UIButton *convertTo  = (UIButton *) sender;
    [convertTo setTitle:buttonText forState:UIControlStateNormal];
    
}


- (IBAction)topEditChaged:(id)sender {
    [self convertToUnits];
}

- (IBAction)bottomEditChanged:(id)sender {
    [self convertToUnits];
}

//setting converted units to desired textbox
- (void) convertToUnits
{
    double top = [self.topFieldText.text doubleValue];
    double bottom = [self.bottomFieldText.text doubleValue];
    double value;
    NSString *result;
    NSInteger selectedIndex = self.selIndex.selectedSegmentIndex;
    
    
    if(self.topToBottom ==YES){
        if(selectedIndex ==0){
            value = [unitConv degreeConvert:top Celcius:YES];
        }
        else if (selectedIndex ==1){
            value = [unitConv distanceConvert:top km:YES];
        }
        else{
            value = [unitConv volumeConvert:top litres:YES];
        }
        result = [NSString stringWithFormat:@"%.4f",value];
        self.bottomFieldText.text = result;
    }
    else
    {
        if(selectedIndex ==0){
            value = [unitConv degreeConvert:bottom Celcius:NO];
        }
        else if (selectedIndex ==1){
            value = [unitConv distanceConvert:bottom km:NO];
        }
        else{
            value = [unitConv volumeConvert:bottom litres:NO];
        }
        
        result = [NSString stringWithFormat:@"%.4f",value];
        self.topFieldText.text = result;
    }
    
}

//Returning the convert button text based on the text field selection
- (NSString *) returnButtonText
{
    NSString *buttonText;
    if(self.topToBottom == YES){
        buttonText =@"▲";
        self.topToBottom = NO;
    }
    else{
        buttonText =@"▼";
        self.topToBottom = YES;
    }
    return buttonText;
}

//clear the numbers when the tab is changed
- (void) clearFields
{
    self.bottomFieldText.text = @"";
    self.topFieldText.text = @"";
}


@end
