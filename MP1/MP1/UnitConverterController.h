//
//  ViewController.h
//  MP1
//
//  Created by Karthik Balasubramanian on 01/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitConverterController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *topFieldText;
@property (weak, nonatomic) IBOutlet UITextField *bottomFieldText;
- (IBAction)buttonInside:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
- (double) degreeConvert:(double)valueToConvert Celcius:(bool)isCelcius;
- (IBAction)tabChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *convertTo;

@property (weak, nonatomic) IBOutlet UISegmentedControl *selIndex;
- (IBAction)convertTo:(id)sender;
@property BOOL topToBottom;
- (IBAction)topEditChaged:(id)sender;
- (IBAction)bottomEditChanged:(id)sender;
@end
