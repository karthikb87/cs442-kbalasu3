//
//  ViewController.m
//  MP3
//
//  Created by Karthik Balasubramanian on 07/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "GameController.h"

@interface GameController ()

@end

@implementation GameController {
    NSMutableArray *pieces;
    int mode;
    BOOL animating;
    int timerCount;
    NSTimer *timer;
}

- (void) awakeFromNib{
    self.gameModel = [[GameModel alloc] init];
    pieces = [[NSMutableArray alloc] init];
}



- (void) setTimer {
    timerCount =0;
    timer = [[NSTimer alloc] init];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countTimer) userInfo:nil repeats:YES];
    
    [timer fire];
    
}

-(void) countTimer{
   
    timerCount++;
    if(timerCount == 10){
        
        [[[UIAlertView alloc] initWithTitle:@"Time Out"
                                    message:@"Random move will be placed."
                                   delegate:self
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil] show];
        timerCount =0;
    }
}

- (void) resetGame{
    [self.gameModel resetGame];
    
    for(UIView * piece in pieces){
        [piece removeFromSuperview];
    }
    [pieces removeAllObjects];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:30];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped)];
    tap.numberOfTapsRequired = 2;
    tap.numberOfTouchesRequired = 2;
    [self.boardView addGestureRecognizer:tap];
    [self doubleTapped];    
}

-(void) doubleTapped{
    [[[UIAlertView alloc] initWithTitle:@"Choose Mode"
                                message:@"10 second timer will be enabled in double player mode for each move."
                               delegate:self
                      cancelButtonTitle:@"Single Player"
                      otherButtonTitles:@"Double Player",nil] show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    NSString *modeTitle =[alertView buttonTitleAtIndex:buttonIndex];

    if([modeTitle isEqualToString:@"Single Player"]){
        mode = 1;
        [self resetGame];
    }
    else if([modeTitle isEqualToString:@"Double Player"]){
        mode =2;
        
        [self resetGame];
        
        [self setTimer];
    }
    
    else if([modeTitle isEqualToString:@"ok"]){
             [timer invalidate];
             int col = rand() % 7;
             [self processTurn:col];
    }
    
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column{
    
    if(mode == 2)
        [timer invalidate];
    
    [self processTurn:column];
    
}

-(void) processTurn:(int) column{
    
    if(animating)
        return;
    
    if([self.gameModel processTurnAtColumn:column]){
        
        UIView *piece = [[UIView alloc] initWithFrame:CGRectMake((column + 1) * self.boardView.gridWidth - self.boardView.slotDiameter / 2.0, - self.boardView.slotDiameter, self.boardView.slotDiameter,self.boardView.slotDiameter)];
        
        [pieces addObject:piece];
        
        piece.backgroundColor = [self.gameModel currentColor]; // Ask for peice and get color
        
        [self.view insertSubview:piece belowSubview:self.boardView];
        
        animating = YES ;
        [UIView animateWithDuration:0.5 animations:^{
            // 5 is the position to get the circle . Need to be dynamic
            piece.center = CGPointMake((column + 1) * self.boardView.gridWidth, (6 - ([self.gameModel topEmptyRowInCol:column] -1 )) * self.boardView.gridHeight);
            
        }completion:^(BOOL finished) {
            animating = NO;
            if(self.gameModel.gameOver){
                
                
                NSString *winnerString ;
                if(mode ==1 && self.gameModel.winner == 2)
                    winnerString = @"Computer Won";
                else if(mode ==1 && self.gameModel.winner == 1)
                    winnerString =@"You Win";
                else
                    winnerString = [NSString stringWithFormat:@"Player %d won",self.gameModel.winner];

                [[[UIAlertView alloc] initWithTitle:@"Game Over"
                                            message:winnerString
                                           delegate:self
                                  cancelButtonTitle:@"Play again"
                                  otherButtonTitles:nil] show];
                
                [self resetGame];
                
                
                //return;
            }
            else {
                if(mode == 1 && _gameModel.turn == 2){
                    
                    //[self getRandomMoveAtColumn:column];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getRandomMoveAtColumn:column];
                        
                    });
                }
                
                if(mode == 2)
                    [self setTimer];
            }
        }];
        
    }
}


-(void) getRandomMoveAtColumn:(int) column{
    int rnd = rand()%5;
    
    if(rnd %3 == 0)
        [self processTurn:column];
    else if(rnd %3 == 1){
        if(column ==1)
            column = 2;
        [self processTurn:column - 1];
    }
    else if(rnd %3 == 2){
        if(column ==6)
            column = 5;
        [self processTurn:column + 1];
    }
    
    
}


@end
