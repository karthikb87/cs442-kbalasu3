//
//  GameModel.h
//  Connect4
//
//  Created by Karthik Balasubramanian on 5/2/14.
//  Copyright (c) 2014 Karthik Balasubramanian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameModel : NSObject

@property (readonly) int turn;
@property (readonly) UIColor *currentColor;
@property (readonly) BOOL gameOver;
@property (readonly) int winner;

- (BOOL) processTurnAtColumn: (int) col;
- (int) topEmptyRowInCol:(int) col;
- (int) pieceForRow:(int) row col:(int) col;
- (void) resetGame;
@end
