//
//  ViewController.h
//  MP3
//
//  Created by Karthik Balasubramanian on 07/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
#import "GameModel.h"

@interface GameController : UIViewController <BoardViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet BoardView *boardView;
@property (strong, nonatomic) GameModel *gameModel;
@end
