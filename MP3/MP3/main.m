//
//  main.m
//  MP3
//
//  Created by Karthik Balasubramanian on 07/05/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
