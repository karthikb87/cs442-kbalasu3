//
//  GameModel.m
//  Connect4
//
//  Created by Karthik Balasubramanian on 5/2/14.
//  Copyright (c) 2014 Karthik Balasubramanian. All rights reserved.
//

#import "GameModel.h"

@implementation GameModel{

    int _turn;
    int _winner;
    int _pieces[6][7];
    int _numPiecesInCol[7];

}

- (id) init {
    if(self = [super init]){
        _turn = 1;
    }
    return self;
}

- (int) turn{
    
    return _turn;
}

- (UIColor *) currentColor{
    
    if(_turn == 1)
        return [UIColor redColor];
    else
        return [UIColor yellowColor];
    
}


- (BOOL) gameOver{
    
    return _winner !=0;
}


- (BOOL) checkWinAtColumn:(int) col WithTurn:(int) turn {
    
    int row = [self topEmptyRowInCol:col];
    if ([self checkWin:row-3 Rowend:row+4 ColStart:col-3 Colend:col+4])
        return YES;
    else
        
        if ([self checkWin:row+3 Rowend:row-4 ColStart:col-3 Colend:col+4])
            return YES;
        else
            
            if ([self checkWin:row Rowend:row ColStart:col-3 Colend:col+4])
                return YES;
            else
                
                if ([self checkWin:row-3 Rowend:row+4 ColStart:col Colend:col])
                    return YES;
    
    return NO;
    
}



- (BOOL) checkWin:(int) startRow Rowend:(int) endRow  ColStart:(int)startCol  Colend:(int)endCol  {
    
        int rowStep = (startRow > endRow ? -1 : (startRow < endRow ? 1 : 0));
        int colStep = (startCol > endCol ? -1 : (startCol < endCol ? 1 : 0));
        int count = 0;
        int row = startRow;
        int col = startCol;
        
        for (; row != endRow || col != endCol; row += rowStep, col += colStep) {
           
            if (row < 0 || col < 0 || row >= 6 || col >= 7)
                continue;
            if (_pieces[row][col] == _turn)
                count ++;
            else
                count = 0;
            
            if (count == 4) break;
        }
        
        return (count == 4);

}
- (int) winner{
    
    return _winner;
}

- (BOOL) processTurnAtColumn:(int)col   {
    
    
    if(_numPiecesInCol[col] == 6 )
        return NO;
    
    
     _numPiecesInCol[col]++;
    
    _pieces[_numPiecesInCol[col]][col]=_turn;
    
    if([self checkWinAtColumn:col WithTurn:_turn])
        _winner = _turn;
    
    
    /*Scenario for game Over
    if(col == 3 && _numPiecesInCol[col] ==3){
        _winner = _turn;
    }*/
    
    _turn = (_turn == 1) ? 2 :1 ;

    return YES;
}


- (int) topEmptyRowInCol:(int)col{
    return _numPiecesInCol[col];
}

- (int) pieceForRow:(int)row col:(int)col{
    return 0;
}

- (void) resetGame{
    _turn =1;
    _winner =0;
    for(int i =0; i< 7 ; i ++)
    {
        _numPiecesInCol[i] = 0;
    }
    
    for(int i =0; i< 7 ; i ++)
    {
        for(int j =0; j< 8 ; j++)
        {
            _pieces[i][j] = 0;
        }
    }
}
@end
