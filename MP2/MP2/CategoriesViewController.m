//
//  MasterViewController.m
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "CategoriesViewController.h"

#import "UnitViewController.h"
#import "UnitConverter.h"
#import "AppDelegate.h"
@interface CategoriesViewController () {
    
}
@end


@implementation CategoriesViewController{
    NSMutableArray *_objects;
    
    NSMutableArray *_categories;
    NSMutableDictionary *_baseTypes;
    UnitConverter *unitConverter;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    unitConverter =[[UnitConverter alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    self.dictFrompList =appDelegate.dictFrompList;
    _categories = self.dictFrompList[@"Categories"];
    _baseTypes = self.dictFrompList[@"BaseTypes"];
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
   
    NSString *categories =_categories[indexPath.row];
    cell.textLabel.text = categories;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
       NSString *cat = _categories[indexPath.row];
        self.detailViewController.types = _dictFrompList[cat];
    }
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *cat = _categories[indexPath.row];
        NSArray *types;
        
        if([cat isEqual:@"Temperature"]){
            types = [unitConverter loadNewDataForTemp:_dictFrompList[cat] withBaseType:_baseTypes[cat]];
        }
        else{
            types = [unitConverter loadNewData:_dictFrompList[cat]]; //[_dictFrompList[cat] allValues];
        }
        
        NSArray *keys = [_dictFrompList[cat] allKeys];
        UnitViewController *detailViewController = [segue destinationViewController];
        detailViewController.title = cat;
        UIBarButtonItem *uiButton =[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        uiButton.title =@"Back";
        detailViewController.navigationItem.backBarButtonItem = uiButton;
        detailViewController.values = types;
        detailViewController.keys = keys;
        detailViewController.baseTypes = _baseTypes;
        detailViewController.subTypes = self.dictFrompList[cat];
        
    }
}

@end
