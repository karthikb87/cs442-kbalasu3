//
//  UnitConverter.h
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnitConverter : NSObject

- (NSArray *) loadNewData:(NSDictionary *) types;
- (NSArray *) loadNewDataForTemp:(NSDictionary *) types withBaseType:(NSString *) baseType;
- (NSArray *) convertValue:(NSDictionary *) types withValue:(double) value withRowId:(int) key withBaseType:(NSString *) baseType;
- (NSArray *) convertValueForTemp:(NSDictionary *) types withValue:(double) value withRowId:(int) key withBaseType:(NSString *) baseType;

@end
