//
//  UnitEditingViewController.h
//  MP2
//
//  Created by Karthik Balasubramanian on 3/26/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitEditingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *baseTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *baseTypeValue;
@property (weak, nonatomic) IBOutlet UITextField *unitNameText;
@property (weak, nonatomic) IBOutlet UITextField *firstFactor;
@property (weak, nonatomic) IBOutlet UITextField *secondFactor;

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *baseType;
@property (weak, nonatomic) IBOutlet UILabel *secondFactorLabel;
@property (strong, nonatomic) NSString *baseValue;
@property (strong) NSMutableDictionary *subTypes;
- (IBAction)doneAdding:(id)sender;
@end
