//
//  DetailViewController.h
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnitEditingViewController.h"
@interface UnitViewController : UITableViewController <UITextFieldDelegate>

- (IBAction)editingChanged:(id)sender;
@property (strong, nonatomic) NSArray *values;
@property (strong, nonatomic) NSArray *keys;
@property (strong, nonatomic) NSMutableDictionary *baseTypes;
@property (strong) NSMutableDictionary *subTypes;
-(IBAction)unwindFromEditingController:(UIStoryboardSegue *)segue;
@end
