//
//  UnitConverter.m
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "UnitConverter.h"

@implementation UnitConverter

- (NSArray *) loadNewData:(NSDictionary *) types
{
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for(id key in types) {
        NSNumber *val = [types objectForKey:key];
        double value = [val doubleValue];
        value = 1/value;
        [newArray addObject:[NSNumber numberWithDouble:value]];
    }
    
    
    return (NSArray *) newArray;
}

- (NSArray *) loadNewDataForTemp:(NSDictionary *) types withBaseType:(NSString *) baseType
{
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    NSNumber *val;
    double baseValue = [types[baseType] doubleValue];
    for(id key in types) {
        if([baseType isEqual:key]){
            val = [types objectForKey:key];
        }
        else{
            NSArray *items =[types objectForKey:key];
            double firstValue = [[items objectAtIndex:0] doubleValue];
            double secondValue = [[items objectAtIndex:1] doubleValue];
            double value = (baseValue - secondValue) / firstValue;
            val = [NSNumber numberWithDouble:value];
        }
        [newArray addObject:val];
    }
    return (NSArray *) newArray;
    
}

- (NSArray *) convertValue:(NSDictionary *) types withValue:(double) value withRowId:(int) key withBaseType:(NSString *) baseType
{
    NSMutableArray *tempArray = (NSMutableArray *)[types allKeys];
    NSString *toKey = [tempArray objectAtIndex:key];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    double baseValue = [[types objectForKey:toKey] doubleValue] * value;
    
    for(id key in types) {
        NSNumber *val = [types objectForKey:key];
        double value = [val doubleValue];
        value =baseValue/value;
        [newArray addObject:[NSNumber numberWithDouble:value]];
    }
    
    return (NSArray *) newArray;
    
}

- (NSArray *) convertValueForTemp:(NSDictionary *) types withValue:(double) value withRowId:(int) key withBaseType:(NSString *) baseType
{
    NSMutableArray *tempArray = (NSMutableArray *)[types allKeys];
    NSString *toKey = [tempArray objectAtIndex:key];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    double baseValue;
    if([toKey isEqual:baseType]){
        baseValue = [types[baseType] doubleValue] * value;
    }
    else
    {
        NSArray *items =[types objectForKey:toKey];
        double firstValue = [[items objectAtIndex:0] doubleValue];
        double secondValue = [[items objectAtIndex:1] doubleValue];
        baseValue = firstValue * value + secondValue;
    }
    

    NSNumber * val;
    for(id key in types) {
        if([toKey isEqual:key]){
            val = [NSNumber numberWithDouble:value];
        }
        else if([key isEqual:baseType]){
             val = [NSNumber numberWithDouble:baseValue];
        }
        else{
            NSArray *items =[types objectForKey:key];
            double firstValue = [[items objectAtIndex:0] doubleValue];
            double secondValue = [[items objectAtIndex:1] doubleValue];
            double value = (baseValue - secondValue) / firstValue;
            val = [NSNumber numberWithDouble:value];
        }

        [newArray addObject:val];
    }
    
    return (NSArray *) newArray;
    
}
@end
