//
//  MasterViewController.h
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UnitViewController;

@interface CategoriesViewController : UITableViewController

@property (strong, nonatomic) UnitViewController *detailViewController;
@property (strong, nonatomic) NSMutableDictionary *dictFrompList;
@end
