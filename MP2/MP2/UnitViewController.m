//
//  DetailViewController.m
//  MP2
//
//  Created by Karthik Balasubramanian on 25/03/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//



#import "UnitViewController.h"
#import "UnitConverter.h"


@interface UnitViewController () {
    NSMutableArray *_objects;
    
}
@end


@implementation UnitViewController{
    UIBarButtonItem *rightbarButton;
    UIBarButtonItem *rightbarAddButton;
    int rowId;
    double value;
    UITextField *uiTextField;
    UnitConverter *unitConverter;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	 
    rightbarButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(updateValues)];
    rightbarAddButton = self.navigationItem.rightBarButtonItem;
    unitConverter= [[UnitConverter alloc] init];
    
}



- (void) updateValues{

    value = [uiTextField.text doubleValue];
    if([self.title isEqual:@"Temperature"]){
        self.values =  [unitConverter convertValueForTemp:self.subTypes withValue:value withRowId:rowId withBaseType:_baseTypes[self.title]];
    }
    else{
        self.values =  [unitConverter convertValue:self.subTypes withValue:value withRowId:rowId withBaseType:_baseTypes[self.title]];
    }
    
    [self.tableView reloadData];
    self.navigationItem.rightBarButtonItem =rightbarAddButton;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.values.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UITextField *textField = (UITextField *)[cell.contentView viewWithTag:1];
    UILabel * label = (UILabel *)[cell.contentView viewWithTag:2];
    
    double rowValue = [self.values[indexPath.row] doubleValue];
    NSString *keyValue = self.keys[indexPath.row];
    
    textField.text = [NSString stringWithFormat:@"%.4lf", rowValue];
    label.text = keyValue;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    UITableViewCell *cell= [self.tableView cellForRowAtIndexPath:indexPath];
    UITextField *textField = (UITextField *) [cell.contentView viewWithTag:1];
    //when u click on the cell text field should be the first responder
    
    textField.text = @"";
    [textField becomeFirstResponder];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.navigationItem.rightBarButtonItem =rightbarButton;
    
    //every textfield will trigger this in order to find which text field is being editted->
    //navigate to the cell of the triggered textfield
    
    UIView *superview = textField.superview;
    while(![superview isKindOfClass:[UITableViewCell class]]) {
        superview = superview.superview;
    }
    rowId = [self.tableView indexPathForCell:(UITableViewCell*)superview].row;
    
    uiTextField = textField;
}



- (IBAction)editingChanged:(id)sender {

    value = [uiTextField.text doubleValue];
    if([self.title isEqual:@"Temperature"]){
        self.values =  [unitConverter convertValueForTemp:self.subTypes withValue:value withRowId:rowId withBaseType:_baseTypes[self.title]];
    }
    else{
        self.values =  [unitConverter convertValue:self.subTypes withValue:value withRowId:rowId withBaseType:_baseTypes[self.title]];
    }
    
    NSMutableArray *cells = [[NSMutableArray alloc] init];
    for (NSInteger j = 0; j < [self.tableView numberOfSections]; ++j)
    {
        for (NSInteger i = 0; i < [self.tableView numberOfRowsInSection:j]; ++i)
        {
            
            [cells addObject:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]]];
        }
    }
    int i=0;
    for (UITableViewCell *cell in cells)
    {
        UITextField *textField = (UITextField *) [cell.contentView viewWithTag:1];
        if(rowId!=i){
            textField.text = [self.values[i] description];
        }
        i++;
    }
    
    /*
    [self.tableView reloadData];
    
    UITableViewCell *cell= [self.tableView cellForRowAtIndexPath:indexP];
    UITextField *textField = (UITextField *) [cell.contentView viewWithTag:1];
    uiTextField = textField;
    [uiTextField becomeFirstResponder];
     */
    
}


// Segues Method
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
        
        UnitEditingViewController  *editController = [segue destinationViewController];
        NSString *baseType =_baseTypes[self.title];
        editController.type = self.title;
        editController.baseType = baseType;
        NSNumber *numbervalue = _subTypes[baseType];
        editController.baseValue = [NSString stringWithFormat:@"%f",[numbervalue doubleValue]];
        editController.title = [NSString stringWithFormat:@"Adding %@ unit" , self.title];
        editController.subTypes = (NSMutableDictionary *)self.subTypes;
        
    
}



-(IBAction)unwindFromEditingController:(UIStoryboardSegue *)segue
{
    UnitEditingViewController *src = segue.sourceViewController;
    self.subTypes = src.subTypes;
    NSArray *types;
    NSString *cat = self.title;
    if([cat isEqual:@"Temperature"]){
        types = [unitConverter loadNewDataForTemp:self.subTypes withBaseType:_baseTypes[cat]];
    }
    else{
        types = [unitConverter loadNewData:self.subTypes];
    }
    
    NSArray *keys = [self.subTypes allKeys];
    
    self.values = types;
    self.keys = keys;
    [self.tableView reloadData];
}


@end
