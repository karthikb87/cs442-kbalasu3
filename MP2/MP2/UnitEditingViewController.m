//
//  UnitEditingViewController.m
//  MP2
//
//  Created by Karthik Balasubramanian on 3/26/14.
//  Copyright (c) 2014 karthikb87. All rights reserved.
//

#import "UnitEditingViewController.h"
#import "AppDelegate.h"
#import "CategoriesViewController.h"
@interface UnitEditingViewController ()

@end

@implementation UnitEditingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.baseTypeLabel.text = self.baseType;
    self.baseTypeValue.text = self.baseValue;
    if(![self.type isEqualToString:@"Temperature"])
    {
        self.secondFactor.hidden = YES;
        self.secondFactorLabel.hidden =YES;
    }
    	// Do any additional setup after loading the view.
}

- (BOOL) addUnit
{
    
    NSString *unitName = self.unitNameText.text;
    NSString *firstfactor = self.firstFactor.text;
    NSString *secondfactor = self.secondFactor.text;
    
    
    if (unitName.length == 0 || firstfactor.length == 0) {
        
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Make sure you fill out all of the information!"
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
        
        return NO;
    }
    
    NSNumber *first = [NSNumber numberWithDouble:[firstfactor doubleValue]];
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
    [dict1 addEntriesFromDictionary:self.subTypes];
    if([self.type isEqualToString:@"Temperature"])
    {
        NSNumber *second = [NSNumber numberWithDouble:[secondfactor doubleValue]];
        NSMutableArray *unitArray = [[NSMutableArray alloc] init];
        [unitArray addObject:first];
        [unitArray addObject:second];
        
        [dict1 setObject:unitArray forKey:unitName];
        
    }
    else
    {
        [dict1 setObject:first forKey:unitName];
    }
    self.subTypes = dict1;
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneAdding:(id)sender {
    [self addUnit];
    [self pushtoPlist];
    [self performSegueWithIdentifier:@"unwindFromEditing" sender:nil];
}

- (void) updateMasterViewController:(NSMutableDictionary *) dictFromPlist
{
    
    //int index = [self.navigationController.viewControllers indexOfObject:self.navigationController.topViewController];
    
    CategoriesViewController *parent = (CategoriesViewController *)[self.navigationController.viewControllers objectAtIndex:0];
    parent.dictFrompList = dictFromPlist;
}
- (void) writePlist:(NSMutableDictionary *) dictToPlist {
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory =  [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"MP2.plist"];
    
    [dictToPlist writeToFile:path atomically:YES];
    //self.dictFrompList = [NSDictionary dictionaryWithContentsOfFile:path];
}
- (void) pushtoPlist
{
    NSMutableDictionary *dictFromPlist = [[NSMutableDictionary alloc]init];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [dictFromPlist addEntriesFromDictionary: appDelegate.dictFrompList];
    [dictFromPlist removeObjectForKey:self.type];
    [dictFromPlist setObject:[self.subTypes copy] forKey:self.type];
    [self updateMasterViewController:dictFromPlist];
    [self writePlist:dictFromPlist];
    
}
@end
